function alertMsg(msg) {
    var closeMethod = '_panModalClose' + new Date().getTime()
    var id = 'panModal_' + new Date().getTime()

    var modalRoot = document.createElement("div");
    modalRoot.innerHTML = `
        <div class="modal is-active">
          <div class="modal-background"  onclick="` + closeMethod + `()"></div>
        
          <div class="modal-content">
            <div class="box">
              <p>` + msg + `</p>
              <!-- Your content -->
            </div>
          </div>
       
          <button class="modal-close is-large" aria-label="close"  onclick="` + closeMethod + `()"></button>
        </div>
    `
    var modalDom = modalRoot
    modalDom.id = id
    document.body.append(modalDom)

    window[closeMethod] = function () {
        document.getElementById(id).outerHTML = ''
    }
    setTimeout(()=>{
        document.getElementById(id).outerHTML = ''
    },1000)
    return id
}


function confirmMsg() {
    var closeMethod = '_panModalClose' + new Date().getTime()
    var id = 'panModal_' + new Date().getTime()

    var modalRoot = document.createElement("div");
    modalRoot.innerHTML = `
                <div class="modal is-active">
                  <div class="modal-background"></div>
                  <div class="modal-card">
                    <header class="modal-card-head">
                      <p class="modal-card-title">Modal title</p>
                      <button class="delete" aria-label="close" onclick="` + closeMethod + `()"></button>
                    </header>
<!--                    <section class="modal-card-body">-->
<!--                      &lt;!&ndash; Content ... &ndash;&gt;-->
<!--                    </section>-->
<!--                    <footer class="modal-card-foot">-->
<!--                      <button class="button is-success">Save changes</button>-->
<!--                      <button class="button">Cancel</button>-->
<!--                    </footer>-->
                  </div>
                </div>
    `
    var modalDom = modalRoot
    modalDom.id = id
    document.body.append(modalDom)

    window[closeMethod] = function () {
        document.getElementById(id).outerHTML = ''
    }
    return id
}